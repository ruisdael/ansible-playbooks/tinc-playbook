# Tinc (VPN) - Ansible Playbook

**Ansible Playbook to install and configure [tinc VPN](https://www.tinc-vpn.org/) 1.1pre18 software in several nodes** 

**[gittrack role](ansible/roles/gittrack/tasks/main.yml)** pushes `/usr/local/etc/tinc/ruisdaelvpn/` contents to https://gitlab.tudelft.nl/ruisdael/tinc-in-ruisdaelvpn, under host-specific changes 

## Requirements (Prior to running playbook) 
* Ansible in controller host
* `tinc 1.1pre18`` be running as tinc VPN server (`ruisdael_vpn`)



## Playbook details

### Ansible roles & tags
* `install` installs and configures tinc VPN
* `users` create user accounts for Ruisdael staff with ssh key in authorized keys 

### hosts (target devices)
Using centralized Ansible inventory in [/etc/ansible/hosts](https://gitlab.tudelft.nl/agoncalvesdeol/ansible-confs/-/blob/main/hosts)
 
### variables
Located in [ansible/playbook.yml](ansible/playbook.yml)

## Run playbook

Playbook can be run for a **new tinc node** or an already **existing tinc node** y
y


### for new tinc node

* performs tinc installation in target host 
* requests a *tinc invitation* from the tinc server. **Tasks under tag `new_invitation`**
* joins the tinc network & create a new tinc config via the *tinc invitation* provided by the server. **Tasks under tag `new_invitation`**

`ansible-playbook -i hosts.yml ansible/playbook.yml --skip-tag retrieve_invitation  --limit davis_00N`


### for existing tinc node
use `--skip-tag new_invitation` to avoid creating a new tinc invitation

* performs tinc installation in target host 
* pulls the previously created tinc config from  [ruisdael/tinc-in-ruisdaelvpn/](https://gitlab.tudelft.nl/ruisdael/tinc-in-ruisdaelvpn/) git repository. **Tasks under tag `retrieve_invitation`**

`ansible-playbook ansible/playbook.yml --skip-tags new_invitation --limit davis_00N`


Tasks under `gittrack` role and `gittrack` task, pushes tinc configurations to [ruisdael/tinc-in-ruisdaelvpn/](https://gitlab.tudelft.nl/ruisdael/tinc-in-ruisdaelvpn/) git repository, 
where they are stored under dedicated [branches](https://gitlab.tudelft.nl/ruisdael/tinc-in-ruisdaelvpn/-/branches) 


# TODO:
`systemctl daemon-reload` after setting services

